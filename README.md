# Redmine

My personal Redmine custom CSS.

## How to use it

You can apply the aformentioned CSS by appending Redmine's one or, if you do not have the access or rights to do so,
use a browser extension or similar to force this CSS on top of Redmine's default one.

### Chrome Extension

For the second case, you can use a browser extension like [Stylebot](https://stylebot.dev/) which you can find in the
Chrome Web Store.

Just add the extension to your browser, then click it on the top (to the right of the address bar) and select
_"Open Stylebot"_. This should open the Stylebot "console".
From here just copy and paste the CSS and it should get added locally.

## Main features

- Easily tweakable thanks to CSS variables. Check the TOP of the css file to find all the variables used and tweak them
at your own liking
- High contrast for better readability
- Rounded edges for most tables and forms in line with Windows 11's styling
- Custom Agile Board look with more recognizable issues priorities and fixed issues card height
- Forced rounded edges on images and max-width for reduced image cluttering

